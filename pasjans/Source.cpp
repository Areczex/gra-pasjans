#include <iostream>
#include <conio.h>
#include <time.h>
#include <cstdlib>
#include <string>
#include "pasjans.h"

using namespace std;

bool aktywnaGra; //odpowiada za dzialanie petli gry, jezeli true to gra jest aktywna
const unsigned int x = 50; // wymiary planszy
const unsigned int y = 80; // wymiary planszy

char Karty[x][y]; // s�u�y do wyswietlania kart w odpowiednich miejscach na planszy
char kopiaKarty[x][y]; // to samo, tylko �e kopia

// ka�da karta posiada na n/w zmienne 
struct elementyTalii
{
	elementyTalii * next, *prev; // nastepna karta, poprzednia
	int wartosc, symbol, numerWiersza, numerKarty, wspX1, wspX2, wspY1, wspY2;
	//wartosc karty 1-13, symbol 1-4, w ktorym wierszu wystepuje karta, -||-, wsp�rz�dna karty lewa gorna, prawa dolna,
};


class Talia
{
public:

	elementyTalii * front, *back; //wskazniki na pierwsz� i ostatni� kart� w talii
	unsigned int licznik, licznikTalii, licznikStosu, X, Y, pX, pY; //licznik,  ile kart w talii|licznikTalii od 1-28(karty w piramidzie), to samo tylko, �e na stosie||| x i y s�u�� do poruszania si� wskaznikiem po planszy, px i py to samo, tylko �e wskazuj� poprzednie miejsce, gdzie by� wska�nik
	char wskaznik;


public:
	//zwykly konstruktor X Y ustawia pocz�tkowe miejsce wskaznika
	Talia()
	{
		front = back = NULL;
		licznik = 0;
		licznikTalii = 0;
		licznikStosu = 28;
		X = 0;
		Y = 30;
		pX = 0;
		pY = 30;
		wskaznik = ' ';

	}
	// dekonstruktor, dop�ki istniej� jakie� karty to kasuj 
	~Talia()
	{
		elementyTalii * p;

		while (front)
		{
			p = front->next;
			delete front;
			front = p;
		}
	}
	// ile kart jest aktualnie w piramidzie 
	unsigned int aktualnaLiczbaKart()
	{
		return licznik;
	}
	// dodaje karte na poczatek talii 
	elementyTalii * dodajNaPoczatek(elementyTalii * p, int symbol, int wartosc)
	{
		p->next = front; p->prev = NULL;
		if (front) front->prev = p;
		front = p;
		if (!back) back = front;
		p->wartosc = wartosc;
		p->symbol = symbol;

		if (licznik < 28)
		{
			licznikTalii++;
		}

		p->numerKarty = licznik;
		ustawWsp(p);
		licznik++;

		return front;
	}
	//to samo, tylko �e na koniec
	elementyTalii * dodajNaKoniec(elementyTalii * p, int symbol, int wartosc)
	{
		if (back) back->next = p;
		p->next = NULL; p->prev = back;
		back = p;
		if (!front) front = back;
		p->wartosc = wartosc;
		p->symbol = symbol;

		if (licznik < 28)
		{
			licznikTalii++;
		}

		p->numerKarty = licznik;
		ustawWsp(p);
		licznik++;
		return back;
	}
	//usuwa konkretn� karte 
	elementyTalii * usunKarte(elementyTalii * p)
	{
		elementyTalii * p1;

		if (p->prev) p->prev->next = p->next;
		else front = p->next;
		if (p->next) p->next->prev = p->prev;
		else back = p->prev;
		licznik--;
		return p;
	}
	//wyswietla informacje o kartach w talii
	void wyswietl()
	{
		elementyTalii * p;

		if (!front)
		{
			cout << "Lista jest pusta." << endl;

		}
		else
		{
			p = front;

			while (p)
			{
				cout << p->wartosc << " ";
				cout << p->symbol << " " << p->numerKarty << " " << endl;
				p = p->next;
			}
			cout << endl;
		}
	}
	// wykorzystywana przy losowaniu wartosci i symbolii dla kart. Je�eli zwraca true to losujemy jeszcze raz jak�� wartosc i symbol
	bool czyIstniejeKarta(int symbol, int wartosc)
	{
		elementyTalii *p;
		bool czyIstnieje;

		if (!front)
		{
			return false;
		}
		else {

			p = front;

			while (p)
			{
				if (p->wartosc == wartosc && p->symbol == symbol)
				{
					return true;
				}
				else {

					p = p->next;
				}
			}
		}
		p = front;

		return false;
	}
	// Tworzymy karty, losujemy dla nich wartosci i symbole.
	void stworzTalie()
	{
		elementyTalii * p;
		int wylosowanaWartosc = 0, wylosowanySymbol = 0;

		while (licznik < 52)
		{
			p = new elementyTalii;
			wylosowanaWartosc = (rand() % 13) + 1;
			wylosowanySymbol = (rand() % 4) + 1;

			if (licznik == 0)
			{
				dodajNaKoniec(p, wylosowanySymbol, wylosowanaWartosc);

			}
			else {

				if (czyIstniejeKarta(wylosowanySymbol, wylosowanaWartosc) == false)
				{
					dodajNaKoniec(p, wylosowanySymbol, wylosowanaWartosc);
				}
			}
		}
		p = new elementyTalii;
		dodajNaKoniec(p, 0, 14);
		p = new elementyTalii;
		dodajNaKoniec(p, 0, 14);

	}
	// Zwraca wartosc karty, wykorzystywana przy rysowaniu kart
	char jakaWartosc(int wartosc)
	{
		char znak;

		switch (wartosc)
		{
		case 1:
			znak = 'A';

			break;

		case 2:
			znak = '2';

			break;

		case 3:
			znak = '3';

			break;

		case 4:
			znak = '4';

			break;

		case 5:
			znak = '5';

			break;

		case 6:
			znak = '6';

			break;

		case 7:
			znak = '7';

			break;

		case 8:
			znak = '8';

			break;

		case 9:
			znak = '9';

			break;

		case 10:
			znak = '1';

			break;

		case 11:
			znak = 'J';

			break;

		case 12:
			znak = 'D';

			break;

		case 13:
			znak = 'K';

			break;

		case 14:
			znak = '@';

			break;

		case 15:
			znak = ' ';

			break;

		}
		return znak;
	}

	// ustawiamy wsp�rz�dne dla kart w talii. Pierwsza karta jest rysowana z Karty[0-3][26-29] itd.
	void ustawWsp(elementyTalii *p)
	{
		switch (licznik)
		{
		case 0:
			p->wspX1 = 26;
			p->wspX2 = 29;
			p->wspY1 = 0;
			p->wspY2 = 3;
			p->numerWiersza = 1;

			break;

		case 1:
			p->wspX1 = 22;
			p->wspX2 = 25;
			p->wspY1 = 4;
			p->wspY2 = 7;
			p->numerWiersza = 2;

			break;

		case 2:
			p->wspX1 = 30;
			p->wspX2 = 33;
			p->wspY1 = 4;
			p->wspY2 = 7;
			p->numerWiersza = 2;

			break;

		case 3:
			p->wspX1 = 18;
			p->wspX2 = 21;
			p->wspY1 = 8;
			p->wspY2 = 11;
			p->numerWiersza = 3;

			break;

		case 4:
			p->wspX1 = 26;
			p->wspX2 = 29;
			p->wspY1 = 8;
			p->wspY2 = 11;
			p->numerWiersza = 3;

			break;

		case 5:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 8;
			p->wspY2 = 11;
			p->numerWiersza = 3;

			break;

		case 6:
			p->wspX1 = 14;
			p->wspX2 = 17;
			p->wspY1 = 12;
			p->wspY2 = 15;
			p->numerWiersza = 4;

			break;

		case 7:
			p->wspX1 = 22;
			p->wspX2 = 25;
			p->wspY1 = 12;
			p->wspY2 = 15;
			p->numerWiersza = 4;

			break;

		case 8:
			p->wspX1 = 30;
			p->wspX2 = 33;
			p->wspY1 = 12;
			p->wspY2 = 15;
			p->numerWiersza = 4;

			break;

		case 9:
			p->wspX1 = 38;
			p->wspX2 = 41;
			p->wspY1 = 12;
			p->wspY2 = 15;
			p->numerWiersza = 4;

			break;

		case 10:
			p->wspX1 = 10;
			p->wspX2 = 13;
			p->wspY1 = 16;
			p->wspY2 = 19;
			p->numerWiersza = 5;

			break;

		case 11:
			p->wspX1 = 18;
			p->wspX2 = 21;
			p->wspY1 = 16;
			p->wspY2 = 19;
			p->numerWiersza = 5;

			break;

		case 12:
			p->wspX1 = 26;
			p->wspX2 = 29;
			p->wspY1 = 16;
			p->wspY2 = 19;
			p->numerWiersza = 5;

			break;

		case 13:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 16;
			p->wspY2 = 19;
			p->numerWiersza = 5;

			break;

		case 14:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 16;
			p->wspY2 = 19;
			p->numerWiersza = 5;

			break;

		case 15:
			p->wspX1 = 6;
			p->wspX2 = 9;
			p->wspY1 = 20;
			p->wspY2 = 23;
			p->numerWiersza = 6;

			break;

		case 16:
			p->wspX1 = 14;
			p->wspX2 = 17;
			p->wspY1 = 20;
			p->wspY2 = 23;
			p->numerWiersza = 6;

			break;

		case 17:
			p->wspX1 = 22;
			p->wspX2 = 25;
			p->wspY1 = 20;
			p->wspY2 = 23;
			p->numerWiersza = 6;

			break;

		case 18:
			p->wspX1 = 30;
			p->wspX2 = 33;
			p->wspY1 = 20;
			p->wspY2 = 23;
			p->numerWiersza = 6;

			break;

		case 19:
			p->wspX1 = 38;
			p->wspX2 = 41;
			p->wspY1 = 20;
			p->wspY2 = 23;
			p->numerWiersza = 6;

			break;

		case 20:
			p->wspX1 = 46;
			p->wspX2 = 49;
			p->wspY1 = 20;
			p->wspY2 = 23;
			p->numerWiersza = 6;

			break;

		case 21:
			p->wspX1 = 2;
			p->wspX2 = 5;
			p->wspY1 = 24;
			p->wspY2 = 27;
			p->numerWiersza = 7;

			break;

		case 22:
			p->wspX1 = 10;
			p->wspX2 = 13;
			p->wspY1 = 24;
			p->wspY2 = 27;
			p->numerWiersza = 7;

			break;

		case 23:
			p->wspX1 = 18;
			p->wspX2 = 21;
			p->wspY1 = 24;
			p->wspY2 = 27;
			p->numerWiersza = 7;

			break;

		case 24:
			p->wspX1 = 26;
			p->wspX2 = 29;
			p->wspY1 = 24;
			p->wspY2 = 27;
			p->numerWiersza = 7;

			break;

		case 25:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 24;
			p->wspY2 = 27;
			p->numerWiersza = 7;

			break;

		case 26:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 24;
			p->wspY2 = 27;
			p->numerWiersza = 7;

			break;

		case 27:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 24;
			p->wspY2 = 27;
			p->numerWiersza = 7;

			break;

		case 28:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 29:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 30:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 31:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 32:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 33:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 34:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 35:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 36:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 37:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 38:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 39:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 40:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 41:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 42:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 43:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 44:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 45:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;


		case 46:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 47:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 48:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;
		case 49:
			p->wspX1 = 34;
			p->wspX2 = 37;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 50:
			p->wspX1 = 42;
			p->wspX2 = 45;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 51:
			p->wspX1 = 50;
			p->wspX2 = 53;
			p->wspY1 = 32;
			p->wspY2 = 35;
			p->numerWiersza = 0;

			break;

		case 52:
			p->wspX1 = 2;
			p->wspX2 = 5;
			p->wspY1 = 32;
			p->wspY2 = 35;

			break;

		case 53:
			p->wspX1 = 26;
			p->wspX2 = 29;
			p->wspY1 = 32;
			p->wspY2 = 35;

			break;

		}
	}

	elementyTalii * index(unsigned n)
	{
		elementyTalii * p;

		if ((n < 0) || (n >= licznik))
		{
			return NULL;
		}
		else
		{
			p = front;
			while (--n + 1) p = p->next;

		}

		return p;
	}

	//rysowanie kart...
	void rysujKarte(int i)
	{
		elementyTalii * p;
		p = index(i);

		for (int i = 0; i < 50; i++)
		{
			for (int j = 0; j < 80; j++)
			{
				if ((i == p->wspY1 && (j > p->wspX1 && j < p->wspX2)) || (i == p->wspY2 && (j > p->wspX1 && j < p->wspX2)))
				{
					Karty[i][j] = '-';
					kopiaKarty[i][j] = '-';

				}
				else if ((i > p->wspY1 && i < p->wspY2) && (j == p->wspX1 || j == p->wspX2))
				{
					Karty[i][j] = '|';
					kopiaKarty[i][j] = '|';

				}
				else if ((i == p->wspY1 && j == p->wspX1) || (i == p->wspY1 && j == p->wspX2) || (i == p->wspY2 && j == p->wspX1) || (i == p->wspY2 && j == p->wspX2))
				{
					Karty[i][j] = jakaWartosc(p->wartosc);
					kopiaKarty[i][j] = jakaWartosc(p->wartosc);

				}
			}
		}

	}
	//stosu...
	void rysujStos(int i)
	{
		elementyTalii * p;
		p = index(i);

		for (int i = 0; i < 50; i++)
		{
			for (int j = 0; j < 80; j++)
			{
				if ((i >= p->wspY1 && (j >= p->wspX1 && j <= p->wspX2)) && (i <= p->wspY2 && (j >= p->wspX1 && j <= p->wspX2)))
				{
					Karty[i][j] = '@';
					kopiaKarty[i][j] = '@';
				}

			}
		}
	}
	//budujemy plansze...
	void zbudujPlansze()
	{
		for (int i = 0; i < 28; i++)
		{
			rysujKarte(i);
		}
		rysujKarte(52);
		rysujStos(53);
	}

	//rysujemy plansze...
	void rysujPlansze()
	{
		for (int i = 0; i < 50; i++)
		{
			for (int j = 0; j < 80; j++)
			{
				cout << Karty[i][j];
			}
			cout << endl;
		}

	}

	elementyTalii * szukajKarty(int y, int x)
	{
		elementyTalii * p;

		p = front;

		for (int i = 0; i < 52; i++)
		{
			if ((p->wspX1 == x) && (p->wspY1 == y))
			{
				break;
			}

			p = p->next;

		}

		return p;
	}

	void przerysujKarte(int idx)
	{
		elementyTalii * p;
		p = index(idx);

		for (int i = 0; i < 50; i++)
		{
			for (int j = 0; j < 80; j++)
			{
				if ((i == p->wspY1 && (j > p->wspX1 && j < p->wspX2)) || (i == p->wspY2 && (j > p->wspX1 && j < p->wspX2)))
				{
					Karty[i][j] = ' ';
					kopiaKarty[i][j] = ' ';

				}
				else if ((i > p->wspY1 && i < p->wspY2) && (j == p->wspX1 || j == p->wspX2))
				{
					Karty[i][j] = ' ';
					kopiaKarty[i][j] = ' ';

				}
				else if ((i == p->wspY1 && j == p->wspX1) || (i == p->wspY1 && j == p->wspX2) || (i == p->wspY2 && j == p->wspX1) || (i == p->wspY2 && j == p->wspX2))
				{
					Karty[i][j] = jakaWartosc(15);
					kopiaKarty[i][j] = jakaWartosc(15);
				}
			}
		}
	}

	// poruszamy sie po planszy wskaznikiem
	void ustawWskaznik(char klawisz)
	{

		switch (klawisz)
		{
		case 'w':
			pX = X;
			pY = Y;
			X -= 1;

			break;

		case 's':
			pX = X;
			pY = Y;
			X += 1;

			break;

		case 'a':
			pY = Y;
			pX = X;
			Y -= 1;

			break;

		case 'd':
			pY = Y;
			pX = X;
			Y += 1;

			break;

		default:

			break;
		}


		//Karty[pX][pY] = kopiaKarty[pX][pY];
		//wskaznik = Karty[X][Y];
		Karty[X][Y] = '*';
		Karty[pX][pY] = kopiaKarty[pX][pY];
	}

};
//funkcja, w kt�rej wykonuj� si� p�tla gry do momentu, a� karty z piramidy b�d� mia�y numerKarty != 0. Do dokonczenia jeszcze
void graj()
{

	Talia talia;
	elementyTalii * p, *p1, *p2, *d, *d1, *d2, *t;
	char klawisz;
	int i = 0;

	aktywnaGra = true;
	talia.stworzTalie();
	talia.zbudujPlansze();
	talia.rysujPlansze();
	//talia.skopiujKarty();

	while (aktywnaGra)
	{
		if (_kbhit)
		{
			klawisz = _getch();

			if (klawisz == 'w')
			{
				talia.ustawWskaznik(klawisz);
			}

			else if (klawisz == 's')
			{
				talia.ustawWskaznik(klawisz);
			}

			else if (klawisz == 'a')
			{
				talia.ustawWskaznik(klawisz);
			}

			else if (klawisz == 'd')
			{
				talia.ustawWskaznik(klawisz);

			}

			else if (klawisz == 32)
			{
				p = talia.szukajKarty(talia.X, talia.Y);
				p1 = talia.index(p->numerKarty + p->numerWiersza);
				p2 = talia.index(p->numerKarty + p->numerWiersza + 1);

				if ((p1->numerKarty > 27 || p1->numerKarty == -1) && (p2->numerKarty > 27 || p2->numerKarty == -1))
				{
					if (p->wartosc == 13)
					{
						talia.przerysujKarte(p->numerKarty);
						p->numerKarty = -1;
					}
					else if (p->wartosc < 13)
					{

						do
						{
							klawisz = _getch();

							if (_kbhit)
							{
								if (klawisz == 'w')
								{
									talia.ustawWskaznik(klawisz);
								}

								else if (klawisz == 's')
								{
									talia.ustawWskaznik(klawisz);
								}

								else if (klawisz == 'a')
								{
									talia.ustawWskaznik(klawisz);
								}

								else if (klawisz == 'd')
								{
									talia.ustawWskaznik(klawisz);

								}

								else if (klawisz == 32)
								{
									d = talia.szukajKarty(talia.X, talia.Y);
									d1 = talia.index(p->numerKarty + p->numerWiersza);
									d2 = talia.index(p->numerKarty + p->numerWiersza + 1);
									if (p->wartosc + d->wartosc == 13)
									{
										talia.przerysujKarte(p->numerKarty);
										talia.przerysujKarte(d->numerKarty);
										p->numerKarty = -1;
										d->numerKarty = -1;

									}
									else break;
								}


							}

							system("cls");
							talia.rysujPlansze();

						} while (klawisz != 32);

					}
				}
			}

			else if (klawisz == 'e')
			{
				for (int i = 0; i < 3; i++)
				{
					talia.rysujKarte(talia.licznikStosu);
					talia.licznikStosu += 1;
				}
			}

			else if (klawisz == 'q')
			{

				do
				{
					klawisz = _getch();

					if (_kbhit)
					{
						if (klawisz == 'w')
						{
							talia.ustawWskaznik(klawisz);
						}

						else if (klawisz == 's')
						{
							talia.ustawWskaznik(klawisz);
						}

						else if (klawisz == 'a')
						{
							talia.ustawWskaznik(klawisz);
						}

						else if (klawisz == 'd')
						{
							talia.ustawWskaznik(klawisz);

						}

						else if (klawisz == 32)
						{
							p = talia.szukajKarty(talia.X, talia.Y);

							talia.przerysujKarte(p->numerKarty);

							p->wspX1 = 2;
							p->wspX2 = 5;
							p->wspY1 = 32;
							p->wspY2 = 35;

							talia.rysujKarte(p->numerKarty);

						}


					}

					system("cls");
					talia.rysujPlansze();

				} while (klawisz != 32);

			}
		}

		system("cls");

		talia.rysujPlansze();

		for (int i = 0; i < 28; i++)
		{
			if (talia.index(i)->numerKarty != -1)
			{
				aktywnaGra = true;
				break;
			}

			else if (i == 27)
			{
				aktywnaGra = false;
			}
		}
	}

}

int main()
{
	srand(time(NULL));

	int menu = 0;


	do
	switch (menu)
	{
	case 0:
		cout << "-------------------------------------------" << endl;
		cout << "|              1. GRA                     |" << endl;
		cout << "|              2. ZASADY                  |" << endl;
		cout << "|              3. STEROWANIE              |" << endl;
		cout << "|              4. WYJSCIE                 |" << endl;
		cout << "-------------------------------------------" << endl;
		cin >> menu;
		system("cls");
		break;

	case 1:
		graj();

		cout << "\n\n\n\n Wprowadz 0, aby powrocic ";

		cin >> menu;
		system("cls");

		break;

	case 2:
		cout << "Pasjans Klatwa Mumii to ciekawy i wspaniale opracowany pasjans\npolegajacy na sciaganiu kart z piramidy.\nAby usunac wszystkie karty nalezy dobierac je parami,\ntak aby ich suma wynosila dokladnie 13."
			<< "\n\nKarty maja nastepujaca punktacje :\n\n\n\n\n\n"
			<< "As - 1 punkt\n"
			<< "Karty 2 - 10 liczone sa wedlug liczby oczek\n"
			<< "Walet - 11 punktow\n"
			<< "Dama - 12 punktow\n"
			<< "Krol - 13 punktow\n\n\n"
			<< "Gdy zabraknie nam mozliwosci utworzenia par z kart z piramidy,\nmozemy wspomoc sie kartami z dolnego stosu.\nKarty na stosie mozna przetasowac tylko jeden raz.\n"
			<< "W tej odmianie pasjansa mozemy korzystac z pustego miejsca\nznajdujacego sie w lewym dolnym rogu.\nNa to miejsce mozemy po�ozyc dowolna karte, a nast�pnie w kazdej\nchwili usun�c ja laczac w pare z inna karta wedlug obowiazujacych zasad.\nJezeli przejrzymy wszystkie karty na stosie i nie sci�gniemy\nwszystkich kart z piramidy nalezy sprobowac jeszcze raz.\n";
		cout << "\n\n\n\n\n Wcisnij 0, aby powrocic ";
		cin >> menu;
		system("cls");

		break;

	case 3:
		cout << "w - gora\ns - dol\na - lewo\nd - prawo\nq - Poloz karte na wolne miejsce\ne - Dobierz karty ze stosu. Mozliwe tylko jedno przejscie stosu w ciagu calej gry";
		cout << "Aby wybrac karte najedz wskaznikiem na lewy gorny rog karty" << endl;
		cout << "\n\n\n\n Wprowadz 0, aby powrocic ";

		cin >> menu;
		system("cls");

		break;

	case 4:

		return 0;
		break;

	} while (menu != 3 || menu != 1);

	system("PAUSE");
}